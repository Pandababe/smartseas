<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'smartsea_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gg)4]>LW})5{PE{&(A:652}Q_sF00JLABr-[%d=Of72@ex/J]&`muWW2vV-BH!@t');
define('SECURE_AUTH_KEY',  '$w+={jqKEAV7[>_r8&iUEcgzGRx%c6yV2(xuA(1fT>{pM=k:p@+,Y4L,R1.+etE>');
define('LOGGED_IN_KEY',    'KI0U^xg1KnS(vS$a9Q6Lb=;!h_61qZHD=_cXu59;l.2SJ*.qO-_4A:wa=_SBxUF~');
define('NONCE_KEY',        '1S@!;,8EtP$Ad<>.^4,S?vjeM>b2{ZDAvR`-Ao;V^Z:bV9%&khyz>+qQceuhvQAt');
define('AUTH_SALT',        '*s+i9($)+~qXp3Bgm8?Ou19IRJ[pPGNfJ= m|a(.mMKTAeOf]Zl+:H&eE8hyBpJ?');
define('SECURE_AUTH_SALT', 'hnTv17k%WKIwX_;f<(_T6lQ@cCw ~G)sAF)V|i.VY1r$wt`JWlDF*S0#? */~AC8');
define('LOGGED_IN_SALT',   'Xhb[XOWtAX@7c]l;2c-fBENHj4mdYMg{{^_g!hg.W?Kn))8?;Vn4J)mMMy/5(mVV');
define('NONCE_SALT',       '_8@8./Ah_Z}>nU^JtQ-ZjT&4jDz>O$Y4y?x7~~Op`]?$%oS$Z g%! w, ,Bo+Z;3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sm6rts36s_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
