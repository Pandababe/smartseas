<?php
	/* Template Name: Sites */

	get_header();
	$current_page_id = get_the_ID();
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<?php $arrWhatWeDo = get_field('field_5a5dd038b0a57', get_option('page_on_front')) ?>
			<div class="col-12">
				<p><?php echo $arrProject[0]['area_label'] ?></p>
			</div>

			<?php $arrWhereAreWe = get_field('field_5a6066ead591d') ?>
			<?php
				$arrDataPoints = [];
				foreach($arrWhereAreWe[0]['graph'] as $arrGraph){
					$arrDataPoints[] = [
						'label'	=> $arrGraph['title'],
						'y'		=> $arrGraph['amount']
					];
				}
			?>
			<h4 class="title m-auto"><?php echo $arrWhereAreWe[0]['label'] ?></h4>
			<div id="chartContainer" class="mb-5" style="height: 370px; width: 100%;"></div>
			<script type="text/javascript">
			window.onload = function() {

				var options = {
					data: [{
							type: "pie",
							startAngle: 45,
							showInLegend: "true",
							legendText: "{label}",
							indexLabel: "{label} ({y})",
							yValueFormatString:"#,##0.#"%"",
							dataPoints: <?php echo json_encode($arrDataPoints) ?>
					}]
				};
				$("#chartContainer").CanvasJSChart(options);

			}
			</script>

			<?php $ctr = 0 ?>
			<?php foreach($arrWhatWeDo[0]['areas'] as $arrArea): ?>
				<?php $ctr ++ ?>
				<div class="col-md-2<?php echo $ctr == 1 ? ' offset-md-' . (6 - count($arrWhatWeDo[0]['areas'])) : '' ?>">
					<div class="welcome-list">
						<a href="<?php echo $arrArea['landing_page'] ?>">
							<img src="<?php echo $arrArea['image']['url'] ?>" />
							<h2><?php echo $arrArea['label'] ?></h2>
						</a>
					</div>
				</div>
			<?php endforeach; ?>

			<div class="clearfix"></div>
			<p>&nbsp;</p>

			<div class="col-md-12">
				<?php echo get_page_content($current_page_id) ?>

				<ul>
					<?php
						$arrDownloads = get_field('field_5a62c53116eb2');
						foreach($arrDownloads as $arrDownload){
				?>
							<li><a href="<?php echo $arrDownload['file']['url'] ?>" target="_blank"><?php echo $arrDownload['file_name'] ?></a></li>
				<?php 
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>