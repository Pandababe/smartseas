<?php
	/* Template Name: Partners */
	get_header();
	$current_page_id = get_the_ID();
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- <ul id="filters" class="clearfix">
					<li><span class="filter active" data-filter=".option1, .option2, .option3">All</span></li>
					<li><span class="filter" data-filter=".option1">option1</span></li>
					<li><span class="filter" data-filter=".option2">option2</span></li>
					<li><span class="filter" data-filter=".option3">option3</span></li>
				</ul> -->
				


				<div id="portfoliolist">
					<div class="portfolio option1" data-cat="option1">
						<?php
							$page = isset($_GET['page']) ? $_GET['page'] : 1;
							$args = array(
								'post_type'			=> 'page',
								'posts_per_page'	=> 10,
								'paged'         	=> $page,
								'meta_query'	=> array(
									array(
										'key'		=> '_wp_page_template',
										'value'		=> 'partners-details.php',
										'compare'	=> '='
									)
								)
							);
							$my_query = new WP_Query($args);

							if($my_query->found_posts):
								while($my_query->have_posts()):
									$my_query->the_post();

									$arrImage = get_field('field_5a82ce901fce5');
									$arrSubheading = get_field('field_5a82ce441fce4');
						?>
									<div class="portfolio-wrapper">				
										<div class="partners-item">
											<div class="row">
												<div class="col-md-7">
													<div class="part-img"><img src="<?php echo $arrImage['url'] ?>" /></div>
												</div>
												<div class="col-md-5 nopadding">
													<div class="part-cont">
														<h3><?php the_title() ?></h3>
														<?php if(!empty($arrSubheading)): ?>
															<h5><?php echo $arrSubheading ?></h5>
														<?php endif; ?>
														<p>
															<?php
																$htmlContent = strip_tags(get_the_content());
																echo strlen($htmlContent) > 232 ? substr($htmlContent, 0, 232) . ' [...]' : $htmlContent;
															?>
														</p>
														<div>
															<a href="<?php the_permalink() ?>" class="btn-more">view project <i class="fa fa-angle-right"></i></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
						<?php
								endwhile;
							endif;
						?>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>