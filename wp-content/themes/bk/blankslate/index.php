<?php
	/* Template Name: Homepage */

	get_header();
	$current_page_id = get_the_ID();

	$arrMapId = ['verde', 'palawan', 'tanon', 'davao', 'lanuza'];
?>

<?php $arrBanners = get_field('field_5a5dccf06140b', get_option('page_on_front')) ?>
<?php if(is_array($arrBanners)): ?>
	<section>
		<div id="banner-wrap">
			<?php $arrLogos = get_field('field_5a5dce93c5412', get_option('page_on_front')) ?>
			<?php if(is_array($arrLogos)): ?>
				<div id="logos">
					<ul>
						<?php foreach($arrLogos as $arrLogo): ?>
							<li><img src="<?php echo $arrLogo['image']['url'] ?>" /></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
			<div class="owl-banner owl-carousel owl-theme">
				<?php foreach($arrBanners as $arrBanner): ?>
					<div>
						<?php if(!empty($arrBanner['caption'])): ?>
							<div class="owl-text-overlay hidden-xs">
								<p class="owl-caption hidden-sm"><?php echo $arrBanner['caption'] ?></p>
							</div>
						<?php endif; ?>
						<img class="owl-img" src="<?php echo $arrBanner['image']['url'] ?>">
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php $arrWhatWeDo = get_field('field_5a5dd038b0a57', get_option('page_on_front')) ?>
<section id="home-abot-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="title"><?php echo $arrWhatWeDo[0]['label'] ?></h4>
				<p><?php echo $arrWhatWeDo[0]['text'] ?></p>
			</div>

			<div class="col-12">
				<p><?php echo $arrProject[0]['area_label'] ?></p>
			</div>

			<?php $ctr = 0 ?>
			<?php foreach($arrWhatWeDo[0]['areas'] as $arrArea): ?>
				<?php $ctr ++ ?>
				<div class="col-md-2<?php echo $ctr == 1 ? ' offset-md-' . (6 - count($arrWhatWeDo[0]['areas'])) : '' ?>">
					<div class="welcome-list">
						<a href="<?php echo $arrArea['landing_page'] ?>">
							<img src="<?php echo $arrArea['image']['url'] ?>" />
							<h2><?php echo $arrArea['label'] ?></h2>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
			
			<div class="col-md-12">
				<br/>
				<a href="<?php echo $arrWhatWeDo[0]['landing_page'] ?>" class="btn-more">read more <i class="fa fa-angle-right"></i></a>
			</div>
		</div>
	</div>
</section>

<?php $arrBanners = get_field('field_5a606f6d29f1a', 220) ?>
<section id="intro-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<h4 class="title"><?php echo get_the_title(220) ?></h4>

				<?php $arrMap = get_field('field_5a63506b8f864', 220) ?>
				<div id="phil-map" class="bg-white">
					<ul>
						<?php for($ctr = 0; $ctr < count($arrMap); $ctr ++): ?>
							<li>
								<div class="loc-list" id="<?php echo $arrMapId[$ctr] ?>">
									<a href="<?php echo get_permalink($arrMap[$ctr]['landing_page']->ID) ?>" class="loc-link"></a>
									<div class="loc-cont">
										<a href="<?php echo get_permalink($arrMap[$ctr]['landing_page']->ID) ?>" class="loc-item"><?php echo $arrMap[$ctr]['landing_page']->post_title ?></a>
										<p><?php echo $arrMap[$ctr]['short_description'] ?></p>
									</div>
									<div class="loc-detail" alt="<?php echo $arrMapId[$ctr] ?>">
										<img src="<?php echo $arrMap[$ctr]['image']['url'] ?>" />
									</div>
								</div>
							</li>
						<?php endfor; ?>
					</ul>
					<?php $arrImage = get_field('field_5a65acaf9c0fb', 220) ?>
					<img src="<?php echo $arrImage['url'] ?>" class="mt-3" />
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	$arrOurStories = get_field('field_5a5f2b08ce291', get_option('page_on_front'));
	$objQuery = new WP_Query([
		'post_type'		=> 'post',
		'posts_per_page'=> 3,
		'meta_query'	=> array(
			array(
				'key'		=> 'feature_on_homepage',
				'value'		=> 'Yes',
				'compare'	=> '='
			)
		)
	]);
	if($objQuery->found_posts):
?>
		<section id="home-stories-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4 class="title"><?php echo $arrOurStories[0]['label'] ?></h4>
						
						<div class="owl-news owl-carousel owl-theme">
						<?php
							while($objQuery->have_posts()):
								$objQuery->the_post();

								$arrFeaturedImage = get_field('field_5a5f2b797463e');
								$arrVideo = get_field('field_5a7af9c0dbc0e');

								$strContent = strip_tags($post->post_content);

								$objCategory = get_the_category($post->ID);
								$strCategoryLink = $objCategory[0]->slug;

								$title = get_the_title();
								$title = strlen($title) > 30 ? substr($title, 0, 30) . '...' : $title;
						?>
							<div class="item">
								<div class="news-post">
									<div class="news-post-detail">
										<div class="news-post-title"><h3><?php echo $title ?></h3></div>
										<div class="news-post-img">
											<?php if(isset($arrFeaturedImage['url'])): ?>
												<img src="<?php echo $arrFeaturedImage['url'] ?>" />
											<?php else: ?>
												<?php echo $arrVideo ?>
											<?php endif; ?>
										</div>
										<div class="news-post-teaser">
											<?php echo substr($strContent, 0, 150) . (strlen($strContent) > 150 ? ' [...]' : '') ?>
										</div>
									</div>
									<a href="<?php echo get_permalink() ?>" class="btn-more">Read more </a>
								</div>
							</div>
						<?php endwhile; ?>
						</div>
					</div>
					
					<div class="col-md-12 text-center">
						<br/>
						<a href="<?php echo site_url() . '/' . $strCategoryLink ?>" class="btn-more">view all <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</section>
<?php endif; ?>

<?php get_footer(); ?>