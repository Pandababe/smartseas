<?php
	/* Template Name: Support Us */

	get_header();
	$current_page_id = get_the_ID();
	$content = get_page_content($current_page_id);
	prp($content);
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<?php if(!empty($content)): ?>
				<div class="col-md-4">
					<div id="phil-locs">
						<?php echo $content ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="col-md-8<?php echo empty($content) ? ' offset-md-2' : '' ?>">
				<?php $arrForm = get_field('field_5a65b167a43b1') ?>
				<form class="mj-wp-forms form-horizontal">
					<h2><?php echo get_field('field_5a65b9144461a') ?></h2>
					<div class="alert alert-success d-none"></div>
					<div class="alert alert-danger d-none"></div>
					<div class="alert alert-info d-none">Please do no leave required (*) fields empty</div>
					<?php foreach($arrForm as $arrKey => $arrField): ?>
						<div class="form-group">
							<label class="control-label"><?php echo ($arrField['required'] ? '* ' : '') . $arrField['label'] ?></label>
							<div class="mb-3">
								<?php
									$arrChoices = explode("\r\n", $arrField['choices']);
									switch($arrField['type']['value']){
										case 'Textarea':
								?>
											<textarea class="form-control" name="mj-field-<?php echo $arrKey ?>"></textarea>
								<?php
											break;

										case 'Dropdown':
								?>
											<select class="form-control" name="mj-field-<?php echo $arrKey ?>">
												<option value="" class="d-none" disabled selected></option>
												<?php foreach($arrChoices as $strChoice): ?>
													<option value="<?php echo $strChoice ?>"><?php echo $strChoice ?></option>
												<?php endforeach; ?>
											</select>
								<?php
											break;

										case 'Textbox':
										default:
								?>
											<input type="text" class="form-control" name="mj-field-<?php echo $arrKey ?>"<?php echo $arrField['required'] ? ' required' : '' ?>  />
								<?php
									}
								?>
							</div>
						</div>
					<?php endforeach; ?>

					<div class="form-group">
						<button type="submit" class="btn btn-primary" name="mj-submit">
							Submit
						</button>
					</div>
				</form>
					
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>