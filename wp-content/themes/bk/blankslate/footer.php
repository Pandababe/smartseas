<section id="newsletter-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="highlight">Subscribe to our weekly Newsletter and stay tuned.</p>
                <form action="" id="newsletter">
                	<div class="alert alert-success d-none"></div>
                	<div class="alert alert-danger d-none"></div>
					<div class="col-md-4 offset-md-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span>
							<input type="email" class="form-control input-lg" name="email" id="email"  placeholder="Enter your Email" required />
						</div>
					</div>
					<br/>
					<button class="btn btn-warning btn-sm" type="submit">Subscribe Now!</button>
              </form>
			</div>
		</div>
	</div>
</section>

<footer id="footer-wrap">
	<!--div class="container">
		<div class="row">
			<div class="col-md-10">
				<div id="navi-foot-wrapper">
					<a href="#">home</a>
					<a href="#">about</a>
					<a href="#">projects</a>
					<a href="#">news</a>
					<a href="#">downloads</a>
					<a href="#">members</a>
					<a href="#">partners</a>
					<a href="#">contact us</a>
				</div>
				
				<address>
					<strong>Twitter, Inc.</strong><br>
					1355 Market St, Suite 900<br>
					San Francisco, CA 94103<br>
					<abbr title="Phone">P:</abbr> (123) 456-7890
				  </address>
				  
				  <div id="social-icn">
				  	<a href="#" id="facebook"><i class="fa fa-facebook"></i></a>
				  	<a href="#" id="twitter"><i class="fa fa-twitter"></i></a>
				  	<a href="#" id="instagram"><i class="fa fa-instagram"></i></a>
				  	<a href="#" id="youtube"><i class="fa fa-youtube"></i></a>
				  	<a href="#" id="google-plus"><i class="fa fa-google-plus"></i></a>
				  	<a href="#" id="rss"><i class="fa fa-rss"></i></a>
				  </div>
				  
				  <p>© 2017 Smartseas</p>
			</div>
			
			<div class="col-md-2">
				<div id="sml-logo">
					<img src="images/misc/smartseas-logo-small.jpg" />
				</div>
			</div>
		</div>
	</div-->
</footer>

<script type="text/javascript">
var template_directory = '<?php echo get_template_directory_uri() ?>';
</script>
<?php wp_footer(); ?>

</body>
</html>