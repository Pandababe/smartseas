<?php
	/* Template Name: Newsletter */

	// spam controller
	session_start();
	$allow = true;
	$waiting_time = 0;
	if(@$_SESSION['last_send_date']){

		$allow = strtotime($_SESSION['last_send_date']) < strtotime(date('Y-m-d H:i:s'));
		$waiting_time = abs(strtotime(date('Y-m-d H:i:s')) - strtotime($_SESSION['last_send_date']));

	}

	if($allow){
		$row = array(
			'field_5a840a3760f68' => $_POST['email'],
			'field_5a840a4660f69' => date('d/m/Y g:i a')
		);

		add_row('field_5a840a1260f67', $row, 367);
		$strNewsletterSuccessMsg = get_field('field_5a840ceacf76d', 367);

		// set last_send_date
		$_SESSION['last_send_date'] = date('Y-m-d H:i:s', time() + 30);
	}
	
	echo json_encode([
		'success'		=> $allow,
		'message'		=> $strNewsletterSuccessMsg,
		'waiting_time'	=> $waiting_time
	]);
?>