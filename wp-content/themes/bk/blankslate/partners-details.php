<?php
	/* Template Name: Partners (Details) */
	get_header();
	$current_page_id = get_the_ID();
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="news-headline"><?php echo the_title() ?></h2>
						<h4><?php echo get_field('field_5a82ce441fce4') ?></h4>

						<?php $arrBanner = get_field('field_5a82ce901fce5') ?>
						<?php if(isset($arrBanner['url'])): ?>
							<p class="text-center"><img src="<?php echo $arrBanner['url'] ?>" /></p>
						<?php endif; ?>
				
						<?php the_content() ?>
						
						<a href="<?php echo '/' . get_url_segment(1) . '/' ?>" class="btn-more btn-back">go back</a>
					<?php endwhile; ?>
				<?php endif; ?>	
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>