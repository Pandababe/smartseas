<?php
	/* Template Name: Projects */

	get_header();
	$current_page_id = get_the_ID();

	$arrMapId = ['verde', 'palawan', 'tanon', 'davao', 'lanuza'];
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div id="phil-locs">
					<?php echo get_page_content($current_page_id) ?>
				</div>
			</div>
			<div class="col-md-8">
				<?php $arrMap = get_field('field_5a63506b8f864', 220) ?>
				<div id="phil-map" class="bg-white">
					<ul>
						<?php for($ctr = 0; $ctr < count($arrMap); $ctr ++): ?>
							<li>
								<div class="loc-list" id="<?php echo $arrMapId[$ctr] ?>">
									<a href="<?php echo get_permalink($arrMap[$ctr]['landing_page']->ID) ?>" class="loc-link"></a>
									<div class="loc-cont">
										<a href="<?php echo get_permalink($arrMap[$ctr]['landing_page']->ID) ?>" class="loc-item"><?php echo $arrMap[$ctr]['landing_page']->post_title ?></a>
										<p><?php echo $arrMap[$ctr]['short_description'] ?></p>
									</div>
									<div class="loc-detail" alt="<?php echo $arrMapId[$ctr] ?>">
										<img src="<?php echo $arrMap[$ctr]['image']['url'] ?>" />
									</div>
								</div>
							</li>
						<?php endfor; ?>
					</ul>
					<?php $arrImage = get_field('field_5a65acaf9c0fb', 220) ?>
					<img src="<?php echo $arrImage['url'] ?>" class="mt-3" />
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>