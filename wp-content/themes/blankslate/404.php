<?php get_header(); ?>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text-center m-5">
					<img src="<?php echo get_template_directory_uri() . '/images/misc/error-404.png' ?>" />
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>