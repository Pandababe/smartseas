<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta name="author" content="Michael Janea" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="1 days" />
<meta name="rating" content="general" />
<meta http-equiv="distribution" content="global" />
<meta http-equiv="content-language" content="en" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="preloader">
    <div id="loader"></div>
</div>

<?php $arrSocialNetworks = get_field('field_5a5dc63419fd2', get_option('page_on_front')) ?>
<?php if(is_array($arrSocialNetworks)): ?>
  <ul id="social-side-links">
    <?php foreach($arrSocialNetworks as $arrSocialNetwork): ?>
    	<li><a style="background-color: <?php echo $arrSocialNetwork['background_color'] ?>;" href="<?php echo $arrSocialNetwork['link'] ?>" target="_blank"><img src="<?php echo $arrSocialNetwork['icon']['url'] ?>" alt="" /></a></li>
    <?php endforeach; ?>
  </ul>
<?php endif; ?>

<header id="head">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="navi-wrapper">
                    <nav class="navbar navbar-expand-lg navbar-light">
                      <?php $arrLogo = get_field('field_5b6546d6e6fa3', get_option('page_on_front')) ?>
                      <a class="navbar-brand" href="<?php echo site_url() ?>"><img src="<?php echo $arrLogo['url'] ?>" /></a>
                     <?php $arrLogos = get_field('field_5a5dce93c5412', get_option('page_on_front')) ?>
      <?php if(is_array($arrLogos)): ?>
        <div id="logos">
          <ul>
            <?php foreach($arrLogos as $arrLogo): ?>
              <li><img src="<?php echo $arrLogo['image']['url'] ?>" /></li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>



                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                      <div class="collapse navbar-collapse" id="navbarNav">
                        <?php
                          wp_nav_menu([
                            'container' => false,
                            'menu_class'=> 'visible-lg'
                          ]);
                        ?>
                      </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>