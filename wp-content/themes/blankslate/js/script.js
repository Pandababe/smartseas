// JavaScript Document

var nua = navigator.userAgent;
var is_android = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));
if(is_android) {
		$('select.form-control').removeClass('form-control').css('width', '100%');

}



/*$(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
		$(".navbar-brand img").css({"width":"111px","height":"118px"});
    } else {
		$(".navbar-brand img").css({"width":"139px","height":"147px"});
    }
});*/

$(function(){

	$('.owl-portfolio').owlCarousel({
		loop:true,
	    margin:10,
	  
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:3
	        }
	    }
	});

	$('.owl-banner').owlCarousel({
		loop:true,
		autoplay:true,
		mouseDrag:false,
		touchDrag:false,
		animateOut: 'fadeOut',
    	animateIn: 'fadeIn',
		smartSpeed:50,
		margin:10,
		nav:false,
		dots:false,
		lazyLoad:true,
		merge:true,
		video:true,
		center:true,
		items:1
	});
	
	$('.owl-news').owlCarousel({
		loop:true,
		autoplay:true,
		margin:10,
		nav:false,
		lazyLoad:true,
		merge:true,
		center:false,
		dots:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:3
			}
		}
	});
	
	$( ".sub-banner-wrap" ).each(function() {
	  var attr = $(this).attr('data-subbanner');

	  if (typeof attr !== typeof undefined && attr !== false) {
		  $(this).css('background', 'url('+attr+') center top no-repeat fixed');
	  }
	});
	
	// $("area[rel^='prettyPhoto']").prettyPhoto();
	// $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: true});
	// $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
	// $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
	// 	custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
	// 	changepicturecallback: function(){ initialize(); }
	// });
	// $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
	// 	custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
	// 	changepicturecallback: function(){ _bsap.exec(); }
	// });
});


jQuery(document).ready(function($){
  window.onload = function (){
    $(".bts-popup").delay(1000).addClass('is-visible');
	}
	//open popup
	$('.bts-popup-trigger').on('click', function(event){
		event.preventDefault();
		$('.bts-popup').addClass('is-visible');
	});
	//close popup
	$('.bts-popup').on('click', function(event){
		if( $(event.target).is('.bts-popup-close') || $(event.target).is('.bts-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$('.bts-popup').removeClass('is-visible');
	    }
    });

    var intActiveMenu = $('#navbarNav').find('.current-menu-item').index();
	$('#navbarNav').find('ul').removeAttr('id').removeAttr('class').addClass('navbar-nav');
	$('#navbarNav').find('li').removeAttr('id').removeAttr('class').addClass('nav-item other-nav');
	if(intActiveMenu >= 0){
		$('#navbarNav').find('li').eq(intActiveMenu).addClass('active');
	}
	$('#navbarNav').find('a').removeAttr('id').removeAttr('class').addClass('nav-link');

	$('.navigation .screen-reader-text').remove();
	$('.nav-links').children().unwrap();
	$('.news-pagination').children().each(function(){
		$(this).wrap('<li class="page-item' + ($(this).prop('tagName') == 'SPAN' ? ' ' + ($(this).hasClass('dots') ? 'disabled' : 'active') : '') + '"></li>');
		if($(this).hasClass('current')){
			$(this).parent().html('<a class="page-link" href="javascript:void(0)">' + $(this).html() + '</a>');
		}else{
			$(this).attr({'class':'page-link'});
		}
	});
	$('.news-pagination').html('<ul class="pagination">' + $('.news-pagination').html() + '</ul>');

	// inquiry form
	$(document).on('submit', '.mj-wp-forms', function(){
		$('.alert').addClass('d-none');
		var objForm = $(this);
		$.ajax({
			type 	: 'post',
			dataType: 'json',
			data 	: $(objForm).serialize(),
			url 	: '/mj-wp-forms',
			success : function(response){
				$(objForm).find('.alert-success').removeClass('d-none').html(response.message);
				$(objForm)[0].reset();
			},
			error	: function(){
				$(objForm).find('.alert-danger').removeClass('d-none').html('An error has occured. Please contact website administrator.');
			}
		});
		return false;
	});

	// newsletter
	$(document).on('submit', '#newsletter', function(){
		$('.alert').addClass('d-none');
		var objForm = $(this);
		$.ajax({
			type 	: 'post',
			dataType: 'json',
			data 	: $(objForm).serialize(),
			url 	: '/mj-wp-newsletter',
			success : function(response){
				if(response.success){
					$(objForm).find('.alert-success').removeClass('d-none').html(response.message);
				}else if(response.waiting_time > 0){
					$(objForm).find('.alert-danger').removeClass('d-none').html('This form requires that you wait 30 seconds between submissions. Please try again in ' + response.waiting_time + ' seconds.');
				}
				$(objForm)[0].reset();
			},
			error	: function(){
				$(objForm).find('.alert-danger').removeClass('d-none').html('An error has occured. Please contact website administrator.');
			}
		});
		return false;
	});
});

$(window).on('load', function(){
	$('#home-video-wrap').find('iframe').attr({'width':'90%', height:'500px'});
	$('iframe').attr({'width':'100%'}).removeAttr('height');
	$('#preloader').fadeOut();
});
