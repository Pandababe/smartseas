<?php
	/* Template Name: Landing */

	get_header();
	$current_page_id = get_the_ID();

	
?>

<?php $arrBanners = get_field('field_5a5dccf06140b', get_option('page_on_front')) ?>
<?php if(is_array($arrBanners)): ?>
	<section>
		<div id="banner-wrap">
			
			<div class="owl-banner owl-carousel owl-theme">
				<?php foreach($arrBanners as $arrBanner): ?>
					<div>
						<?php if(!empty($arrBanner['caption'])): ?>
							<div class="owl-text-overlay hidden-xs">
								<p class="owl-caption hidden-sm"><?php echo $arrBanner['caption'] ?></p>
							</div>
						<?php endif; ?>
						<img class="owl-img" src="<?php echo $arrBanner['image']['url'] ?>">
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php $arrWhatWeDo = get_field('field_5a5dd038b0a57', get_option('page_on_front')) ?>
<section id="home-abot-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="title"><?php echo $arrWhatWeDo[0]['label'] ?></h4>
				<p><?php echo $arrWhatWeDo[0]['text'] ?></p>
			</div>

			<div class="col-12">
				<p><?php echo $arrProject[0]['area_label'] ?></p>
			</div>

			<?php $ctr = 0 ?>
			<?php foreach($arrWhatWeDo[0]['areas'] as $arrArea): ?>
				<?php $ctr ++ ?>
				<div class="col-md-2<?php echo $ctr == 1 ? ' offset-md-' . (6 - count($arrWhatWeDo[0]['areas'])) : '' ?>">
					<div class="welcome-list">
						<a href="<?php echo $arrArea['landing_page'] ?>">
							<img src="<?php echo $arrArea['image']['url'] ?>" />
							<h2><?php echo $arrArea['label'] ?></h2>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		
		</div>
	</div>
</section>

<?php $arrBanners = get_field('field_5a606f6d29f1a', 220) ?>




<?php
	$arrOurStories = get_field('field_5a5f2b08ce291', get_option('page_on_front'));
	$objQuery = new WP_Query([
		'post_type'		=> 'post',
		'posts_per_page'=> 3,
		'meta_query'	=> array(
			array(
				'key'		=> 'feature_on_homepage',
				'value'		=> 'Yes',
				'compare'	=> '='
			)
		)
	]);
	if($objQuery->found_posts):
?>
		<section id="home-stories-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4 class="title"><?php echo $arrOurStories[0]['label'] ?></h4>
						
						<div class="owl-news owl-carousel owl-theme">
						<?php
							while($objQuery->have_posts()):
								$objQuery->the_post();

								$arrFeaturedImage = get_field('field_5a5f2b797463e');
								$arrVideo = get_field('field_5a7af9c0dbc0e');

								$strContent = strip_tags($post->post_content);

								$objCategory = get_the_category($post->ID);
								$strCategoryLink = $objCategory[0]->slug;

								$title = get_the_title();
								$title = strlen($title) > 30 ? substr($title, 0, 30) . '...' : $title;
						?>
							<div class="item">
								<div class="news-post">
									<div class="news-post-detail">
										<div class="news-post-title"><h3><?php echo $title ?></h3></div>
										<div class="news-post-img">
											<?php if(isset($arrFeaturedImage['url'])): ?>
												<img src="<?php echo $arrFeaturedImage['url'] ?>" />
											<?php else: ?>
												<?php echo $arrVideo ?>
											<?php endif; ?>
										</div>
										<div class="news-post-teaser">
											<?php echo substr($strContent, 0, 150) . (strlen($strContent) > 150 ? ' [...]' : '') ?>
										</div>
									</div>
									<div class="col-md-12 text-center">
									<br/>
									<a href="<?php echo get_permalink() ?>" class="btn-more">Read more </a>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						</div>
					</div>
					
					<div class="col-md-12 text-center">
						<br/>
						<a href="<?php echo site_url() . '/' . $strCategoryLink ?>" class="btn-more">view all <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</section>
<?php endif; ?>


<section class="subpage-wrap">
	<div class="container">
			<h4 class="title">Our Portfolio</h4>
					<div class="owl-portfolio owl-carousel owl-theme">
						<?php
							$args = array(
								'post_type'			=> 'page',
								'posts_per_page'	=> 100,
							
								'meta_query'	=> array(
									array(
										'key'		=> '_wp_page_template',
										'value'		=> 'partners-details.php',
										'compare'	=> '='
									)
								)
							);
							$my_query = new WP_Query($args);

							if($my_query->found_posts):
								while($my_query->have_posts()):
									$my_query->the_post();
									$arrImage = get_field('field_5a82ce901fce5');
									$arrSubheading = get_field('field_5a82ce441fce4');
						?>

											
									<div class="portfolio-wrapper">														
										<a href="<?php echo get_permalink();?>">		
													<div class="part-img"><img src="<?php echo $arrImage['url'] ?>" /></div>
											

											
													<div class="part-cont">
														<h3><?php the_title() ?></h3>
														<?php if(!empty($arrSubheading)): ?>
															<h5><?php echo $arrSubheading ?></h5>
														<?php endif; ?>
														<p>
															<?php
																$htmlContent = strip_tags(get_the_content());
																echo strlen($htmlContent) > 232 ? substr($htmlContent, 0, 232) . ' [...]' : $htmlContent;
															?>
														</p>

											
												</div>
												</a>
												</div>
						<?php
								endwhile;
							endif;
						?>
					</div>
			
		
</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">

<div class="col-md-8 offset-md-2">
								<form class="mj-wp-forms form-horizontal">
					<h2>We would like to hear from you!</h2>
					<div class="alert alert-success d-none"></div>
					<div class="alert alert-danger d-none"></div>
					<div class="alert alert-info d-none">Please do no leave required (*) fields empty</div>
											<div class="form-group">
							<label class="control-label">* First Name</label>
							<div class="mb-3">
																			<input type="text" class="form-control" name="mj-field-0" required="">
															</div>
						</div>
											<div class="form-group">
							<label class="control-label">* Last Name</label>
							<div class="mb-3">
																			<input type="text" class="form-control" name="mj-field-1" required="">
															</div>
						</div>
											<div class="form-group">
							<label class="control-label">* Email Address</label>
							<div class="mb-3">
																			<input type="text" class="form-control" name="mj-field-2" required="">
															</div>
						</div>
											<div class="form-group">
							<label class="control-label">* Mobile Number</label>
							<div class="mb-3">
																			<input type="text" class="form-control" name="mj-field-3" required="">
															</div>
						</div>
											<div class="form-group">
							<label class="control-label">* Message</label>
							<div class="mb-3">
																			<textarea class="form-control" name="mj-field-4"></textarea>
															</div>
						</div>

					
					<div class="form-group">
						<button type="submit" class="btn btn-primary" name="mj-submit">
							Submit
						</button>
					</div>
				</form>
					
			</div>
				
					
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>