<section id="newsletter-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="highlight">Subscribe to our weekly Newsletter and stay tuned.</p>
                <form action="" id="newsletter">
                	<div class="alert alert-success d-none"></div>
                	<div class="alert alert-danger d-none"></div>
					<div class="col-md-4 offset-md-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></span>
							<input type="email" class="form-control input-lg" name="email" id="email"  placeholder="Enter your Email" required />
						</div>
					</div>
					<br/>
					<button class="btn btn-warning btn-sm" type="submit">Subscribe Now!</button>
              </form>
			</div>
		</div>
	</div>
</section>

<footer id="footer-wrap">

	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<h2>Sitemap</h2>
				<div id="navi-foot-wrapper">
					<p><a href="/">Home</a></p>
					<p><a href="/sites">Sites</a></p>
					<p><a href="/downloads">Downloads</a></p>
					<p><a href="http://cleanseas.smartseas.ph">Cleanseas PH</a></p>
				</div>
			</div>
			<div class="col-md-4">
				<h2>Address</h2>	
				<address>
				SMARTSeas PH Project Management Unit Building 7 Ninoy Aquino Parks and Wildlife Center North Avenue, Quezon City 1103
				  </address>
			</div>
			<div class="col-md-3">	 
				<h2>Social Links</h2> 
				  <div id="social-icn">
				  	<a href="https://www.facebook.com/smartseasph/" id="facebook"><i class="fa fa-facebook"></i></a>
				  	<a href="#" id="twitter"><i class="fa fa-twitter"></i></a>
				  	<a href="#" id="instagram"><i class="fa fa-instagram"></i></a>
				  	<!-- <a href="#" id="youtube"><i class="fa fa-youtube"></i></a>
				  	<a href="#" id="google-plus"><i class="fa fa-google-plus"></i></a>
				  	<a href="#" id="rss"><i class="fa fa-rss"></i></a> -->
				  </div>
			</div>	  
			
			<div class="col-md-2">
				<div id="sml-logo">
						<p style="width: 300px; margin: 0 auto;">
	<?php $arrLogo = get_field('field_5b6546d6e6fa3', get_option('page_on_front')) ?>
                      <a class="" href="<?php echo site_url() ?>"><img style="width: 100%; height: auto;" src="<?php echo $arrLogo['url'] ?>" /></a>
	</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript">
var template_directory = '<?php echo get_template_directory_uri() ?>';
</script>
<?php wp_footer(); ?>

</body>
</html>