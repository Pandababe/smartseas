<?php
	get_header();
	$current_page_id = get_the_ID();
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php echo get_page_content($current_page_id) ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>