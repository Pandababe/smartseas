<?php
	/* Template Name: Sites */

	get_header();
	$current_page_id = get_the_ID();
	$arrMapId = ['verde', 'palawan', 'tanon', 'davao', 'lanuza'];
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>



<section id="intro-wrap">
	<div class="container">
	
				<h4 class="title"><?php echo get_the_title(220) ?></h4>

				<?php $arrMap = get_field('field_5a63506b8f864', 220);
				?>
				<div class="scroll-bar">
				<div id="phil-map" class="bg-white">
					<ul>
						<?php for($ctr = 0; $ctr < count($arrMap); $ctr ++): 
						?>
							<li>
								<div class="loc-list" id="<?php echo $arrMapId[$ctr] ?>">
								<!-- <?php echo get_permalink($arrMap[$ctr]['landing_page']->ID) ?> -->
									<a href="" class="loc-link"></a>
									<div class="loc-cont">
										<p class="loc-item"><?php echo $arrMap[$ctr]['landing_page']->post_title ?></p>
										<p><?php echo $arrMap[$ctr]['short_description'] ?></p>
										<p><?php echo $arrMap[$ctr]['project_website'] ?></p>
										<p><?php echo $arrMap[$ctr]['others'] ?></p>
									</div>
									<!-- <div class="loc-detail" alt="<?php echo $arrMapId[$ctr] ?>">
										<img src="<?php echo $arrMap[$ctr]['image']['url'] ?>" />
									</div> -->
								</div>
							</li>
						<?php endfor; ?>
					</ul>
					<?php $arrImage = get_field('field_5a65acaf9c0fb', 220) ?>
					<img src="<?php echo $arrImage['url'] ?>" class="mt-3" />
				</div>
		</div>

	</div>
</section>
<?php get_footer() ?>