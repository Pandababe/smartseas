<?php get_header() ?>

<section id="news-page" class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title-primary"><span>News and events</span></h2>
			</div>
			
			<div class="col-md-9">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="news-headline"><?php echo the_title() ?></h2>
						
						<?php $arrBanner = get_field('field_5a5f2c5b5331b') ?>
						


						

						<?php 
						$test  = get_field('field_5a7af9c0dbc0e');
						echo '<pre>';
						print_r($test);
						echo '</pre>';
						 ?>
						 <style>
						 iframe {
							height: 50vh;	
						 }
						 </style>

						<?php if(isset($arrBanner['url'])): ?>
							<p class="text-center"><img src="<?php echo $arrBanner['url'] ?>" /></p>
						<?php endif; ?>
				
						<?php the_content() ?>
						
						<a href="<?php echo '/' . get_url_segment(1) . '/' ?>" class="btn-more btn-back">go back</a>
					<?php endwhile; ?>
				<?php endif; ?>	
			</div>
			
			<div class="col-md-3">
				<div class="list-right">
					<ul>
						<li>Category</li>
						<?php foreach(get_categories() as $objCategory): ?>
							<li><a href="<?php echo '/' . $objCategory->slug . '/' ?>"><?php echo $objCategory->name ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				
				<div id="archive" class="list-right">
					<ul>
						<li>Archive</li>
						<?php echo wp_get_archives() ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>