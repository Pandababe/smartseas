<?php get_header() ?>

<section id="news-page" class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title-primary"><span>News and events</span></h2>
			</div>
			
			<div class="col-md-9">
				<?php
					$page = isset($_GET['page']) ? $_GET['page'] : 1;
					$args = array(
						'post_type'			=> 'post',
						'posts_per_page'	=> 10,
						'paged'         	=> $page,
						'orderby'			=> 'meta_value_num',
						'meta_key'			=> 'date',
						'meta_query'		=> [
							[
								'key'		=> 'date',
								'value' 	=> get_url_segment(1) . '-' . get_url_segment(2),
								'compare'	=> 'like',
								'type'		=> 'date'
							]
						]
					);
					$my_query = new WP_Query($args);

					if($my_query->found_posts):
				?>
						<div class="news-items">
							<ul>
								<?php while($my_query->have_posts()): $my_query->the_post(); ?>
									<li>
										<div class="news-item">
											<div class="news-title-teas"><a href="<?php echo get_permalink() ?>"><?php the_title() ?></a></div>
											<div class="news-date"><?php echo date('F j, Y', strtotime(get_field('field_5a62a9db15854'))) ?></div>
										</div>
									</li>
								<?php endwhile; ?>
							</ul>
						</div>
				<?php endif; ?>
				
				<nav aria-label="news-pagination" class="news-pagination">
					<?php
						echo paginate_links([
                            'base'      => '?page=%#%',
                            'current'   => max(1, $_GET['page']),
                            'total'     => $my_query->max_num_pages
						])
					?>
				</nav>
			</div>
			
			<div class="col-md-3">
				<div id="archive" class="list-right">
					<ul>
						<li>Archive</li>
						<?php echo wp_get_archives() ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>