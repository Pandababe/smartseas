<?php
	/* Template Name: Downloads */

	get_header();
	$current_page_id = get_the_ID();
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="containr">
		<div class="row">
			<div class="col-md-12">
				<ul class="download-list">
					<?php
						$arrDownloads = get_field('field_5a62c53116eb2');
						foreach($arrDownloads as $arrDownload){
				?>
							<li><a href="<?php echo $arrDownload['file']['url'] ?>" target="_blank"><?php echo $arrDownload['file_name'] ?></a></li>
				<?php 
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php get_footer() ?>