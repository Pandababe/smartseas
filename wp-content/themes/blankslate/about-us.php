<?php
	/* Template Name: About Us */

	get_header();
	$current_page_id = get_the_ID();
?>

<?php $arrBanners = get_field('field_5a606f6d29f1a', $current_page_id) ?>
<section>
	<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php the_title() ?></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<?php $arrWhoWeAre = get_field('field_5a606fcb29f1b', $current_page_id) ?>
			<div class="col-md-12">
				<h2 class="title-primary"><span><?php echo $arrWhoWeAre[0]['label'] ?></span></h2>
				<?php echo $arrWhoWeAre[0]['content'] ?>
				<br/>
				
				<h2 class="title-primary"><span>how we work</span></h2>
				<p>Quisque mattis in eros porta egestas. Fusce faucibus pharetra metus non ullamcorper. Aenean id dolor ut lorem suscipit venenatis et vel ipsum. Vivamus lobortis mauris quis magna pharetra ullamcorper. Aliquam blandit turpis nec nunc aliquet luctus. Duis sed facilisis neque, sit amet sagittis lacus. Cras condimentum leo quis arcu vestibulum cursus. </p>
			</div>
			
			<?php $arrHowWeWork = get_field('field_5a60702529f1e', $current_page_id) ?>
			<?php $ctr = 0 ?>
			<?php foreach($arrHowWeWork[0]['areas'] as $arrHow): ?>
				<?php $ctr ++ ?>
				<div class="col-md-2<?php echo $ctr == 1 ? ' offset-md-' . (6 - count($arrHowWeWork[0]['areas'])) : '' ?> text-center">
					<div class="welcome-list">
						<a href="<?php echo $arrHow['landing_page'] ?>">
							<img src="<?php echo $arrHow['image']['url'] ?>" />
							<h2><?php echo $arrHow['label'] ?></h2>
							<p><?php echo $arrHow['caption'] ?></p>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
			
			<div class="col-md-12">
				<?php echo get_page_content($current_page_id) ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>