<?php
	/*
	Template Name: Cleanseas
	*/
	get_header(); 
	$current_page_id = get_the_ID();
	$arrBanners = get_field('field_5a606f6d29f1a', $current_page_id)
?>
	<section>
		<div class="sub-banner-wrap" data-subbanner="<?php echo $arrBanners['url'] ?>">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2><?php the_title() ?></h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="subpage-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
			<h5>facebook</h5>
			<?php echo do_shortcode('[custom-facebook-feed]'); ?>
			</div>
			<div class="col-md-4">
				<h5>Tweeter</h5>
			<?php echo do_shortcode('[custom-twitter-feeds]'); ?>
			
			</div>

			<div class="col-md-4">
<h5>gallery</h5>
<div class="owl-gall owl-carousel owl-theme">
			<?php $arrgallery = get_field('field_5b5fbb2492ed6', $current_page_id);
				foreach($arrgallery as $items) { ?>
					
					<div class="item"><img class="alignnone size-full wp-image-543" src="<?php echo $items['images']; ?>" alt="" width="576" height="410" /></div>
			<?php	}
			?>
</div>
</div>
		</div>
	</div>
</section>

<?php
	get_footer();
?>

<script>
	$('.owl-gall').owlCarousel({
		loop:true,
		autoplay:true,
		margin:0,
		nav:true,
		lazyLoad:true,
		merge:true,
		center:false,
		dotClass: 'owl-dot',
		dotsClass: 'owl-dots',
		dots:true,
		items:1
	});
	$( ".owl-prev").html('<i class="fa fa-angle-left fa-3x"></i>');
 	$( ".owl-next").html('<i class="fa fa-angle-right fa-3x"></i>');
</script>